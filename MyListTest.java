public class MyListTest {
    public static void main(String[] args) {
        List<Integer> intList = new List<>();
        intList.add(5);
        intList.add(4);
        intList.add(7);
        intList.add(9);
        intList.add(1);

        System.out.println("Size: " + intList.getSize());
        System.out.println("2-ci indexdeki element: " + intList.get(2));
        intList.remove(2);
        System.out.println("silindikden sonraki list " + intList);
        System.out.println("ucuncu elementin indexi : " + intList.indexOf(9));
        System.out.println("elementin olub olmamasi: " + intList.indexOf(3));
        System.out.println("elementi silmek 1i " + intList.remove(Integer.valueOf(1)));
        System.out.println("elementi sildikden sonraki hal: " + intList);
        intList.add(6);
        intList.add(7);
        intList.add(8);
        System.out.println("sort dan evvelkiki hal: " + intList);
        intList.sort();
        System.out.println("sort dan sonraki hal: " + intList);

    }
}
