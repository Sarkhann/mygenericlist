import java.util.Arrays;
import java.util.Optional;

public class List<T> {
    private T[] elements;
    private int size;

    public List() {
        elements = (T[]) new Object[10];
    }

    public void add(T element) {
        if (size == elements.length) {
            elements = Arrays.copyOf(elements, elements.length * 2);
        }
        elements[size] = element;
        size++;
    }

    public T get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + "Size: " + size);
        }
        return elements[index];
    }

    public T remove(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException("Index: " + index + "Size: " + size);
        }
        else {
            T deteleValue = (T) elements[index];
            for (int i = index; i <size-1 ; i++) {
                elements[i] = elements[i+1];
            }
            elements[size - 1] = null;
            size--;
            return deteleValue;
        }
    }

    public int getSize() {
        return size;
    }

    public int indexOf(T element) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                return i;
            }
        }
        return -1;
    }

    public boolean contains(T element) {
        for (int i = 0; i < size; i++) {
            if (elements[i].equals(element)) {
                return true;
            }
        }
        return false;
    }

    public boolean removeElement(T element) {
        int index = indexOf(element);
        if (index >= 0) {
            remove(index);
            return true;
        }
        return false;
    }

    public List<T> sort() {
        Arrays.sort((T[]) elements, 0, size);
        return this;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < size; i++) {
            if (elements[i] != null) {
                sb.append(elements[i]);
                if (i < size - 1) {
                    sb.append(", ");
                }
            } else {
                sb.append("null");
                if (i < size - 1) {
                    sb.append(", ");
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }


}
